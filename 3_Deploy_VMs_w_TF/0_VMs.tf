
##################################################################################
# PROVIDERS
##################################################################################

#vSphere Provider

provider "vsphere" {
    user = var.vsphere_provider_user
    password = var.vsphere_provider_password
    vsphere_server = var.vsphere_provider_server  

# if using a self signed cert:
    allow_unverified_ssl = true
}

##################################################################################
# DATA
# (this tells terraform to query vCenter and bring back some data)
#
##################################################################################
#
##################
# Inventory data
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/datacenter
###


data "vsphere_datacenter" "dc" {
    name = var.vsphere_dc_name  
}

data "vsphere_host" "cl1_host" {
  count         = length(var.esxi_hosts)
  name          = var.esxi_hosts[count.index]
  datacenter_id = data.vsphere_datacenter.dc.id
}

##################
# VMs & Template data
###

data "vsphere_virtual_machine" "template_w2019" {
    
    name = var.template_name
    datacenter_id = data.vsphere_datacenter.dc.id  
}

##################
# Resource Pool data
###

data "vsphere_resource_pool" "RP1HIGH" {
  name                    = "RP1HIGH"
  datacenter_id = data.vsphere_datacenter.dc.id 

}

data "vsphere_resource_pool" "RP2STANDARD" {
  name                    = "RP2STANDARD"
  datacenter_id = data.vsphere_datacenter.dc.id 
}

data "vsphere_resource_pool" "RP3LOW" {
  name                    = "RP3LOW"
  datacenter_id = data.vsphere_datacenter.dc.id 
}


##################
# Network data
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/network
###

data "vsphere_network" "MGT_DIR_VLAN101" {
  name                            = "DIR_MGT_VLAN101"
  datacenter_id = data.vsphere_datacenter.dc.id 
}
data "vsphere_network" "MGT_SAN_VLAN102" {
  name                            = "MGT_SAN_VLAN102"
  datacenter_id = data.vsphere_datacenter.dc.id 
}
data "vsphere_network" "MGT_DB_VLAN103" {
  name                            = "MGT_DB_VLAN103"
  datacenter_id = data.vsphere_datacenter.dc.id 
}
data "vsphere_network" "MGT_TOOL_VLAN104" {
  name                            = "MGT_TOOL_VLAN104"
  datacenter_id = data.vsphere_datacenter.dc.id 
}
data "vsphere_network" "MGT_WEB_VLAN105" {
  name                            = "MGT_WEB_VLAN105"
  datacenter_id = data.vsphere_datacenter.dc.id 
}
data "vsphere_network" "MGT_RDS_VLAN106" {
  name                            = "MGT_RDS_VLAN106"
  datacenter_id = data.vsphere_datacenter.dc.id 
}

##################
# Storage data
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/datastore
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/datastore_cluster
###

data "vsphere_datastore" "LOCAL01" {
    name = "LOCAL01"
    datacenter_id = data.vsphere_datacenter.dc.id  
}

data "vsphere_datastore" "LOCAL02" {
    name = "LOCAL02"
    datacenter_id = data.vsphere_datacenter.dc.id  
}

data "vsphere_datastore" "iso_datastore" {
    name = "LOCAL01"
    datacenter_id = data.vsphere_datacenter.dc.id  
}







##################################################################################
# RESOURCES
##################################################################################
#
##################
# VMs
#https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine
##################
###
# VMs from Template
###

  resource "vsphere_virtual_machine" "domain_controllers" {
     
     count = length(var.domain_controllers)
     name = element(var.domain_controllers, count.index)
     resource_pool_id = data.vsphere_resource_pool.RP1HIGH.id
     datastore_id = data.vsphere_datastore.LOCAL02.id

     num_cpus = 1
     memory = 512
     guest_id = data.vsphere_virtual_machine.template_w2019.guest_id
     scsi_type = data.vsphere_virtual_machine.template_w2019.scsi_type
     
     network_interface {
       network_id = data.vsphere_network.MGT_DIR_VLAN101.id
       adapter_type = data.vsphere_virtual_machine.template_w2019.network_interface_types[0]
     }

     disk {
       label = "DC01_DISK0.vmdk"
       size = data.vsphere_virtual_machine.template_w2019.disks.0.size
       eagerly_scrub = data.vsphere_virtual_machine.template_w2019.disks.0.eagerly_scrub
       thin_provisioned = data.vsphere_virtual_machine.template_w2019.disks.0.thin_provisioned
     }
     clone {
       template_uuid = data.vsphere_virtual_machine.template_w2019.id
       customize {
           network_interface {
             ipv4_address = element(var.domain_controller_ips, count.index)
             ipv4_netmask = 24
             dns_domain = "test.local"
             dns_server_list = var.dns_servers
           }

           ipv4_gateway = var.default_gateway
            windows_options {
                computer_name  = element(var.domain_controllers, count.index)
                workgroup      = "test"
                admin_password = var.win_dc_password
            }    
       }
     }
 }
