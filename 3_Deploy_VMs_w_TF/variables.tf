##################################################################################
# VARIABLES
##################################################################################
#
##################
# Provider Variables
######

variable "vsphere_provider_user" {
  default = "administrator@vsphere.local" # Note that the defaults listed in your variable.tf will be over-ridden by whatever you state in your TFvars file.
}
variable "vsphere_provider_password" {} # Note that where you have nothing between your curly brackets/braces, you will need to have this variable listed in your TFVars file or alternatively you will be prompted for the value when you run plan, apply or destroy.
variable "vsphere_provider_server" {}

##################
# vCenter & Datacenter Variables
######

variable "vsphere_dc_name" {}

##################
# ESXi Host Variables
######

variable "esxi_hosts" {
  default = [
    "ESX701",
    #"ESX702",    
  ]
}
variable "vsphere_host_user" {
  default = "root"
}
variable "vsphere_host_password" {}

variable "esxi_thumbprints" {}

variable "dvs_network_interfaces" {
  default = [
    "vmnic2",
    "vmnic3",
  ]
}

##################
# VM Variables
######

variable "template_name" {
  default = "Win2019-Template-Base-Thin"
}

variable "win_dc_password" {}

variable "win_srv_password" {}

variable "domain_controllers" {}

variable "domain_controller_ips" {}

variable "member_servers" {}

variable "member_server_ips" {}

variable "dns_servers" {}

variable "default_gateway" {}



