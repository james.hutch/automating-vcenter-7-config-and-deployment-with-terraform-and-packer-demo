# README 
https://www.packer.io/

## PART 3 of 3 - Deploy VMs from our Server 2019 template using Terraform

### Pre-Reqs

We'll need a working Server 2019 template with VMware Tools installed. Part 2 covered deploying such a template using Packer.

# Terraform Content

* [0_VMs.tf](.\0_vCenter.tf) - the main terraform file that in this case contains:
    - the provider (vsphere) 
    - the data for all of the existing resources that we created in Part 1
    - and all of the resources we will be creating. 
* [variables.tf](.\variables.tf) - a separate terraform file for the variables.
* [vcenter_DEV.tfvars](..\\tfvars_file_example\\vcenter_DEV.tfvars) - another variables file that is specific to the the environment we are deploying, in the case "Development".

# Terraform Deployment

1. The tfvars file [vcenter_DEV.tfvars](..\\tfvars_file_example\\vcenter_DEV.tfvars) is the same one we used in Part 1. This should already be updated with all of the correct config, but if you want to change the VM names / IPs or add new ones in, you can do that now.

2. Initialize
```
cd .\3_Deploy_VMs_w_TF\
terraform init
```
Hopefully no errors!

3. Plan 
```
terraform plan -var-file="..\tfvars_file_example\vcenter_DEV.tfvars"
```
Again, hopefully no errors and if so we will see terraform generate a plan based on what actions to take.

4. Apply
```
terraform apply -var-file="..\tfvars_file_example\vcenter_DEV.tfvars" -auto-approve 
```
Using the -auto-approve switch negates the need to check and confirm -obviously we wont do this in production but its a prompt way for the purpose of this demo.
You should get an "Apply complete!" message after about 10 seconds of scrolling text. 

5. Log in to your vCenter have a browse around, and you should now see that we have all of the resources as described at the top of this readme.

6. RDP
RDP to your example domain controller VMs just to check if they work... you will note that no domain has been created and that these VMs were not promoted to DCs - not in scope of this example - sorry.

7. DESTROY!
Whenever you're ready to clean up, feel free to run terraform destroy to delete your VMs
```
terraform destroy -var-file="..\tfvars_file_example\vcenter_DEV.tfvars" -auto-approve
```
Should take a lot less time than actually creating the VMs.
