##################################################################################
# PROVIDERS
##################################################################################

#vSphere Provider

provider "vsphere" {
    user = var.vsphere_provider_user
    password = var.vsphere_provider_password
    vsphere_server = var.vsphere_provider_server  

# if using a self signed cert:
    allow_unverified_ssl = true
}

##################################################################################
# DATA
# (this tells terraform to query AWS and bring back some data)
##################################################################################

# No  data required for this deployment

##################################################################################
# RESOURCES
##################################################################################
#
##################
# vSphere Inventory
##################
# Datacenter
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/datacenter
###

resource "vsphere_datacenter" "dc" {
  name = var.vsphere_dc_name
}
output "vsphere-datacenter" {
  value = vsphere_datacenter.dc.name
}

##################
# Folders
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/folder
###

resource "vsphere_folder" "template_folder" {
  path          = "Templates"
  type          = "vm"
  datacenter_id = vsphere_datacenter.dc.moid
}

output "vsphere_folder" {
  value = vsphere_folder.template_folder.path
}

##################
# Hosts and Clusters
##################
# Hosts
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/host
###

# You can add hosts individually...
/*
resource "vsphere_host" "ESX701" {
  hostname   = "ESX1.yourdomain.local"
  username   = "root"
  password   = "U53a83t7£R1!"
  cluster = vsphere_compute_cluster.Cluster01.id
   thumbprint = "55:72:a2:67:ad:00:fd:0d:5b:8a:13:f6:b6:b0:6d:52:11:56:f3:eb"
}

resource "vsphere_host" "ESX702" {
  hostname   = "ESX1.yourdomain.local"
  username   = "root"
  password   = "U53a83t7£R1!"
  cluster = vsphere_compute_cluster.Cluster01.id
   thumbprint = "47:4d:72:ed:54:3f:4d:90:e0:3b:a1:eb:05:09:d2:bb:d4:b0:4f:bd"
}
*/

# or by variables, lists and counts:
resource "vsphere_host" "cl1_host" {
  count         = length(var.esxi_hosts)
  hostname          = var.esxi_hosts[count.index]
  
  username   = var.vsphere_host_user
  password   = var.vsphere_host_password
  cluster    = vsphere_compute_cluster.Cluster01.id
  thumbprint = var.esxi_thumbprints[count.index]
}

##################
# Clusters
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/compute_cluster
###

resource "vsphere_compute_cluster" "Cluster01" {
  name            = "Cluster01"
  
  datacenter_id   = vsphere_datacenter.dc.moid
  host_managed = true

  drs_enabled          = true
  drs_automation_level = "manual"

  ha_enabled = false
}
output "vsphere_cluster" {
  value = vsphere_compute_cluster.Cluster01.name
}

##################
# Resource Pools
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/resource_pool
###

resource "vsphere_resource_pool" "RP1HIGH" {
  name                    = "RP1HIGH"
  parent_resource_pool_id = vsphere_compute_cluster.Cluster01.resource_pool_id

}

resource "vsphere_resource_pool" "RP2STANDARD" {
  name                    = "RP2STANDARD"
  parent_resource_pool_id = vsphere_compute_cluster.Cluster01.resource_pool_id
}

resource "vsphere_resource_pool" "RP3LOW" {
  name                    = "RP3LOW"
  parent_resource_pool_id = vsphere_compute_cluster.Cluster01.resource_pool_id
}

##################
#Network
##################
# vSwitches
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/host_virtual_switch
###

# Use vSwicth1 for vMotion... however, Terraform doesnt yet appear to support vmkernel adapters. Maybe a job for ansible?
resource "vsphere_host_virtual_switch" "vSwitch1" {
  name = "vSwitch1"
  host_system_id = vsphere_host.cl1_host.0.id
  network_adapters = [ "vmnic4","vmnic5" ]

  active_nics = [ "vmnic4","vmnic5" ]
  standby_nics = []
  
}

# Additional hosts need to be manually specified, unless this is added to the host profile..? 
/*
resource "vsphere_host_virtual_switch" "vSwitch1" {
  name = "vSwitch1"
  host_system_id = vsphere_host.cl1_host.1.id
  network_adapters = [ "vmnic1" ]

  active_nics = [ "vmnic1" ]
  standby_nics = []
  
}
*/

##################
# Port Groups for Host vSwitch
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/host_port_group
###
/*
resource "vsphere_host_port_group" "examplePG" {
  name                = "examplePG"
  host_system_id      = vsphere_host.cl1_host.1.id
  virtual_switch_name = vsphere_host_virtual_switch.vSwitch1.name

  vlan_id = 200
  #allow_promiscuous = true
}
*/

##################
# Distributed vSwitches
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/distributed_virtual_switch
###

resource "vsphere_distributed_virtual_switch" "cl1_dvs" {
  name          = "cl1_dvs1"
  datacenter_id = vsphere_datacenter.dc.moid

  uplinks         = ["uplink1","uplink2"]
  active_uplinks  = ["uplink1","uplink2"]
  standby_uplinks = []

  host {
    host_system_id = vsphere_host.cl1_host.0.id
    devices        = [var.dvs_network_interfaces[0],var.dvs_network_interfaces[1]]
  }
# Additional Hosts are needed to be added manually :(
  /*
  host {
    host_system_id = vsphere_host.cl1_host.1.id
    devices        = [var.dvs_network_interfaces[0]]
  }
  */
}

##################
# Distributed Port Groups
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/distributed_port_group
###

resource "vsphere_distributed_port_group" "MGT_DIR_VLAN101" {
  name                            = "DIR_MGT_VLAN101"
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.cl1_dvs.id

  #vlan_id = 101 # vlan id commented out here in this demo as we will use this to connect a VM later - setting up the actual vlans is not in scope. 
}
resource "vsphere_distributed_port_group" "MGT_SAN_VLAN102" {
  name                            = "MGT_SAN_VLAN102"
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.cl1_dvs.id

  vlan_id = 102
}
resource "vsphere_distributed_port_group" "MGT_DB_VLAN103" {
  name                            = "MGT_DB_VLAN103"
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.cl1_dvs.id

  vlan_id = 103
}
resource "vsphere_distributed_port_group" "MGT_TOOL_VLAN104" {
  name                            = "MGT_TOOL_VLAN104"
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.cl1_dvs.id

  vlan_id = 104
}
resource "vsphere_distributed_port_group" "MGT_WEB_VLAN105" {
  name                            = "MGT_WEB_VLAN105"
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.cl1_dvs.id

  vlan_id = 105
}
resource "vsphere_distributed_port_group" "MGT_RDS_VLAN106" {
  name                            = "MGT_RDS_VLAN106"
  distributed_virtual_switch_uuid = vsphere_distributed_virtual_switch.cl1_dvs.id

  vlan_id = 106
}


##################
# STORAGE
##################
# Datastores
#https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/vmfs_datastore
###

# examples of how we could create datastores although we wont go in to that in this demo.

/*
data "vsphere_vmfs_disks" "available" {
  host_system_id = vsphere_host.ESX702.id
  rescan         = true
  #filter         = ""
}
*/
/*
resource "vsphere_vmfs_datastore" "ESX702_LOCAL01" {
  name           = "ESX702_LOCAL01"
  host_system_id = vsphere_host.ESX702.id
  
  disks = [data.vsphere_vmfs_disks.available.disks[1]]
  
}
resource "vsphere_vmfs_datastore" "ESX702_LOCAL02" {
  name           = "ESX702_LOCAL02"
  host_system_id = vsphere_host.ESX702.id
  
  disks = [data.vsphere_vmfs_disks.available.disks[2]]
  
}
*/
/*
resource "vsphere_vmfs_datastore" "ESX702_LOCAL01" {
  name = "ESX702_LOCAL01"
  host_system_id = vsphere_host.ESX702.id
  disks = [ 
    "mpx.vmhba0:C0:T1:L0",
    "mpx.vmhba0:C0:T2:L0"
          ]
}
*/

##################
# VMs
# https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine
##################

# No VMs in this deployment - this will happen in Part 3 of 3

