# PART 1 of 3 - Deploy and Configure VMware vCenter & ESXi Terraform 

This process is automated and repeatable and can be built on to deploy additional vCenter config.

At a high level, the config in its current form will create and configure the following:

* 1x vCenter Datacenter
* 1x Cluster
* Join an ESXi host to the cluster
* 3x Resource Pools 
* 1x Host vSwitch uplinked to the host's 2nd network adapter (vmnic1). For this demo, we're not creating any host port groups
* 1x Distributed vSwitch uplinked to the hosts 3rd and 4th network adapters (vmnic2, vmnic3 - as configured in variables.tf)
* 6x Distributed Port Groups

No VMs will be deployed yet; this will happen in Part 3.

## Pre-Reqs

In order to get this to work we will first need.

* [Windows Server 2019 Trial ISO download] (https://www.microsoft.com/en-us/evalcenter/evaluate-windows-server-2019)

* 1x ESXi Host (this can be hosted on VMware Workstation 16)
  - 10-12 logical processors if possible
  - at least 16 GB memory
  - Hard Disk1 32GB - ESXi OS
  - Hard Disk1 100GB - create a datastore and deploy VCSA here, also upload the Server 2019 iso to a new "ISOs" folder on this datastore.
  - Hard Disk1 100GB -  create another datastore; this will be used for deploying the VMs to in part 3.
  - 4x Network Adpaters (in "Bridge" mode if using VMWA Workstation)

* 1x VCSA deployed to the above ESXi host (thin provisioned!)

Log in to vCenter to have a look at how blank and empty it is - dont join your host yet; we're going to do that using Terraform in the next step.

# Terraform Content

* [0_vCenter.tf](./0_vCenter.tf) - the main terraform file that in this case contains the provider (vsphere) and all of the resources we will be creating.
* [variables.tf](./variables.tf) - a separate terraform file for the variables.
* [vcenter_DEV.tfvars](../tfvars_file_example/vcenter_DEV.tfvars) - another variables file that is specific to the the environment we are deploying, in the case "Development".

# Terraform Deployment

1. Using a code editor update the [vcenter_DEV.tfvars](../tfvars_file_example/vcenter_DEV.tfvars) with the config specific to your vCenter and ESXi host, specifically the vCenter and ESXi host DNS names or IP addresses and vCenter and ESXi admin/root usernames and passwords. We will use this same tfvars file in part 3 when we deploy the VMs.

2. Initialize
```
  cd .\1_Set_up_vCenter_w_TF\
  terraform init
```
Hopefully no errors!

3. Plan 
```
terraform plan -var-file="..\tfvars_file_example\vcenter_DEV.tfvars"
```
Again, hopefully no errors and if so we will see terraform generate a plan based on what actions to take.

4. Apply
```
terraform apply -var-file="..\tfvars_file_example\vcenter_DEV.tfvars" -auto-approve
```
Using the -auto-approve switch negates the need to check and confirm -obviously we wont do this in production but its a prompt way for the purpose of this demo.
You should get an "Apply complete!" message after about 10 seconds of scrolling text. 

5. Log in to your vCenter have a browse around, and you should now see that we have all of the resources as described at the top of this readme.

6. DESTROY!
Obviously dont do this in production, but simply for the purpose of demonstration:
```
terraform destroy -var-file="..\tfvars_file_example\vcenter_DEV.tfvars" -auto-approve
```
And as simple as that its gone, well just the config at least; your ESXi and VCSA are still there untouched and up and running, but vCenter is back to blank again. This was possible at this stage because no VMs have yet been created which would in effect lock various resources that we have created here (and then subsequently destroyed).

7. Re-apply so we have something to deploy the VMs to.
```
terraform apply -var-file="..\tfvars_file_example\vcenter_DEV.tfvars" -auto-approve
```
Log in to vCenter and check its all back...

8. Now move on to Part 2 where we will use Packer to create a VM template on our vCenter, which will then enable us to actually deploy the VMs.

