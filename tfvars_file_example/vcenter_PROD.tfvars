##################################################################################
# PRODUCTION VARIABLES
##################################################################################
#
##################
# Provider Variables
######

vsphere_provider_user = "administrator@vsphere.local" # vCenter admin username (used in Part 1 and 3)
vsphere_provider_password = "enter_your_vCenter_password_here" # (used in Part 1 and 3)
vsphere_provider_server = "PROD_vcsa01" # vCenter DNS name or IP address (used in Part 1 and 3)

##################
# vCenter & Datacenter Variables
######

vsphere_dc_name  = "Demo-Datacenter" # vCenter Datacenter name (used in Part 1 and 3)

##################
# ESXi Host Variables
######

esxi_hosts = [ # List of hosts (used in Part 1 and 3)
    "PROD_esx1", # Enter you Host IPs or DNS names. Note: You will need to be able to resolve names from vCenter if using DNS names of course
    #"PROD_esx2", # You can add more ESXi hosts to this list as required, but you will need to manually add these hosts to DvSwitch(es)
  ]
vsphere_host_user = "root" # (used in Part 1)
vsphere_host_password = "esxi_password_here" # (used in Part 1)


esxi_thumbprints = [ # (used in Part 1) The certificate thumbprint can be found in the details tab of the ESXi Web GUI's Self signed Certificate
    "55:72:a2:67:ad:00:fd:0d:5b:8a:13:f6:b6:b0:6d:52:11:56:f3:eb",
    #"b6:7e:a4:33:8c:13:6d:b5:41:1a:aa:4c:7b:da:c4:17:ba:b7:07:b5", # if adding more ESXi hosts above, be sure to include the certifcate thumbprints
  ]

/*
dvs_network_interfaces = [ # Comment out or delete TFVAR variables, if you want to use the default in variables.tf
    "vmnic2",
    "vmnic3",
  ]
*/

##################
# VM Variables
######

#template_name = "Server2019_template" # (used in Part 3) Comment out or delete TFVAR variables if you want to use the default in variables.tf

win_dc_password = "domain_controller_password_here" # this will be the local administrator password for the domain controllers (used in Part 3)

domain_controllers = [ # DC computer names
    "PROD_DC01",
    "PROD_DC02",
   ]
domain_controller_ips = [ # DC IP addresses
    "10.0.1.1",
    "10.0.1.2",
   ]

win_srv_password = "secure_password" # (Not Used)

member_servers = [ # (Not Used)
    "PROD_DB01",
    "PROD_WEB01",
   ]

member_server_ips = [ # (Not Used)
    "10.0.1.33",
    "10.0.1.34",
   ]

dns_servers = [
    "10.1.1.1",
    "10.1.1.2" 
  ]

default_gateway = "10.0.1.254"