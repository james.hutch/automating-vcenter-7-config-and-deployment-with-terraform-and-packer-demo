# README 
https://www.packer.io/

## PART 2 of 3 - Deploy a Server 2019 Server to vCenter using Packer

### Pre-Reqs

#### Install Packer (w chocolatey)
Install Chocolatey:
```
https://chocolatey.org
```

Run choco install for the packer package:
```
cinst -y packer
```

Check the Packer version
```
packer version
```
### Packer Template Content 

The Packer content here is an excerpt taken from [Sam Gabrail's git repo] (https://github.com/samgabrail/packer-terraform-vmware), which has been slightly tweaked to build our 2019 demo template.


### Usage

Update the .\variables.json file with the config specific to your vCenter.

```
cd .\win2019.base
packer build -var-file=".\variables.json" .\win2019.baseThin.json
```

[Sam Gabrail's YouTube video on Packer VMware templates](https://www.youtube.com/watch?v=jn4fa7XC_7g) goes through the usage in more depth.

### Packer templates for Windows Server using vSphere-ISO provider

VMware vCenter builder documentation
https://www.packer.io/docs/builders/vmware/vsphere-iso

This repository contains **HashiCorp Packer** templates to deploy **Windows Server** distros in **VMware vSphere (with vCenter)**, using the **vsphere-iso** builder.

These templates creates the Template (or VM) directly on the vSphere server and install the latest VMware Tools.

### Content:

Windows Server 2019:
* [win2019.base/win2019.baseThin.json](./win2019.base/win2019.baseThin.json) --> Windows Server 2019 Packer JSON file Base
* [win2019.base/win2019.baseThin.json](./win2019.base/win2019.baseThin.json) --> Packer JSON Variables file - update this file with the details specific to your environment.
* [win2019.base/autounattend.xml](./win2019.base/autounattend.xml) --> Answer file for unattended Windows setup

Scripts:
* [scripts/disable-network-discovery.cmd](./scripts/disable-network-discovery.cmd) --> Script to Disable network discovery
* [scripts/disable-winrm.ps1](./scripts/disable-winrm.ps1) --> Script to Disable WinRM
* [scripts/enable-rdp.cmd](./scripts/enable-rdp.cmd) --> Script to Enable Remote Desktop
* [scripts/enable-winrm.ps1](./scripts/enable-winrm.ps1) --> Script to Enable WinRM
* [scripts/install-vm-tools.cmd](./scripts/install-vm-tools.cmd) --> Script to Install VMware Tools
* [scripts/set-temp.ps1](./scripts/set-temp.ps1) --> Script to Set Temp Folders

Tested with **VMware ESX 7.0U1** | User: Administrator | Password: S3cr3t0!


# Credits: #

Thanks to Sam Gabrial and inspired by [Stefan Scherer](https://github.com/StefanScherer/packer-windows), [Mischa Taylor](https://sheska.com/automating-windows-server-2016-installs) and [Dmitry Teslya](https://dteslya.engineer/automation/2018-12-20-creating_vm_templates_with_packer) works.
https://medium.com/@gmusumeci/how-to-use-packer-to-build-a-windows-server-template-for-vmware-vsphere-3bc0dc9852ed
https://github.com/guillermo-musumeci/packer-vsphere-iso-windows
