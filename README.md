# DEMO: Automating vCenter 7 config and deployment with Terraform and Packer

## Intro

This Demo will take you through the basics of using Terraform and Packer to automate the config and deployment of a simple vCenter cluster and a couple of VMs.

## What's in the Demo

Follow the readme's sequentially in each of the subfolders:
* [1_Set_up_vCenter_w_TF](./1_Set_up_vCenter_w_TF) - Step 1: Use Terraform to create a Cluster vCenter with some resource pools and a DvSwitch, add a host to the cluster.
* [2_create_win_template_w_packer](./2_create_win_template_w_packer) - Step 2: Use Packer to deploy a pre-configured Window 2019 Server template in to vCenter
* [3_Deploy_VMs_w_TF](./3_Deploy_VMs_w_TF) - Step 3: Use Terrform to deploy a couple of VMs to vCenter using the 2019 server we created in step 2.
